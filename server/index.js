const path = require('path');
const app = require('koa')();
const IO = require('koa-socket');
const serve = require('koa-static');
const controllers = require('./controllers');

const io = new IO();

io.attach(app);

app.use(serve(path.join(__dirname, '..', 'public')));

controllers(app);

module.exports = app;
