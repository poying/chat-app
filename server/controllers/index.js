const requireDirectory = require('require-directory');
const controllers = requireDirectory(module);

module.exports = (app, router) => {
  Object.keys(controllers)
    .forEach(key => controllers[key](app, router))
};
