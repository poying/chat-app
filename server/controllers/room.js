module.exports = app => {
  const sessions = {};
  const session = ctx => {
    sessions[ctx.socket.id] = sessions[ctx.socket.id] || {};
    return sessions[ctx.socket.id];
  };

  app.io.on('updateInfo', ctx => {
    session(ctx).info = ctx.data;
    ctx.socket.emit('updateInfoSuccess');
  });

  app.io.on('getInfo', ctx => {
    ctx.socket.emit('info', session(ctx).info);
  });

  app.io.on('join', ctx => {
    session(ctx).room && ctx.socket.socket.leave(ctx.data.room);
    ctx.socket.socket.join(ctx.data.room);
    session(ctx).room = ctx.data.room;
  });

  app.io.on('sendMessage', ctx => {
    app._io.to(session(ctx).room).emit('message', {
      from: session(ctx).info,
      body: ctx.data,
      time: Date.now()
    });
  });
};
