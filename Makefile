BUILD := node_modules/.bin/browserify
SRC = $(wildcard client/*.js)

start: build
	@node app

build: public/build.js

public/build.js: $(SRC)
	@$(BUILD) client/index.js -o $@ -t [ babelify --presets [ es2015 react ] ]
