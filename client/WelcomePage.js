import React, { Component } from 'react';

class WelcomePage extends Component {
  constructor() {
    super();
    this.state = { name: 'room1' };
  }

  joinRoom(e) {
    e.preventDefault();
    const path = this.context.router.createPath(`/room/${this.state.name}`);
    this.context.router.push(path);
  }

  render() {
    return (
      <div>
        <form onSubmit={this.joinRoom.bind(this)}>
          <input
            value={this.state.name}
            placeholder="room"
            onChange={e => this.setState({ name: e.target.value })} />
          <input type="submit" value="Join" />
        </form>
      </div>
    );
  }
}

WelcomePage.contextTypes = {
  router: React.PropTypes.object.isRequired
};

export default WelcomePage;
