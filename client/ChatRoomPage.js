import React, { Component } from 'react';

const Message = props => (
  <div>{props.message.from.name}: {props.message.body}</div>
);

const MessageList = props => {
  const messages = props.messages
    .sort((a, b) => a.time - b.time)
    .map((message, i) => <Message key={i} message={message} />);
  return <div>{messages}</div>;
};

class UserInfoForm extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      proccessing: false
    };
  }

  updateUserInfo(e) {
    e.preventDefault();
    this.setState({ proccessing: true }, () => {
      this.context.socket.once('updateInfoSuccess', () => {
        this.setState({ proccessing: false });
        this.props.onUpdated();
      });
      this.context.socket.emit('updateInfo', { name: this.state.name });
    });
  }

  render() {
    return (
      <form onSubmit={this.updateUserInfo.bind(this)}>
        <input type="text" placeholder="Your name" onChange={e => this.setState({ name: e.target.value })} />
        <input type="submit" value="Update" />
      </form>
    );
  }
}

UserInfoForm.contextTypes = {
  socket: React.PropTypes.object
};

class ChatRoom extends Component {
  constructor() {
    super();
    this.state = {
      message: '',
      messages: []
    };
  }

  componentDidMount() {
    this.context.socket.on('message', message => {
      const newMessages = this.state.messages.concat(message)
        .sort((a, b) => a.time - b.time);
      this.setState({ messages: newMessages });
    });
  }

  sendMessage(e) {
    e.preventDefault();
    this.context.socket.emit('sendMessage', this.state.message);
    this.setState({ message: '' });
  }

  render() {
    return (
      <div>
        <MessageList messages={this.state.messages} />
        <form onSubmit={this.sendMessage.bind(this)}>
          <input
            type="text"
            value={this.state.message}
            onChange={e => this.setState({ message: e.target.value })} />
          <input type="submit" value="Send" />
        </form>
      </div>
    );
  }
}

ChatRoom.contextTypes = {
  socket: React.PropTypes.object
};

const Loading = props => <div>Loading...</div>;

class ChatRoomPage extends Component {
  constructor() {
    super();
    this.state = {
      user: null,
      fetching: false
    };
  }

  componentDidMount() {
    this.join();
    this.fetchUserInfo();
  }

  join() {
    this.context.socket.emit('join', { room: this.props.params.name });
  }

  fetchUserInfo() {
    this.setState({ fetching: true }, () => {
      this.context.socket.once('info', user => this.setState({ user, fetching: false }));
      this.context.socket.emit('getInfo');
    });
  }

  render() {
    const content = this.state.fetching
      ? <Loading />
      : this.state.user
      ? <ChatRoom user={this.user} />
      : <UserInfoForm onUpdated={this.fetchUserInfo.bind(this)} />;
    return <div>{content}</div>;
  }
}

ChatRoomPage.contextTypes = {
  socket: React.PropTypes.object
};

export default ChatRoomPage;
