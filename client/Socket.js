import React, { Component } from 'react';

class Socket extends Component {
  constructor(props) {
    super();
    const socket = io.connect(props.server);
    this.state = { socket };
  }

  getChildContext() {
    return { socket: this.state.socket };
  }

  render() {
    return this.props.children;
  }
}

Socket.childContextTypes = {
  socket: React.PropTypes.string
};

export default Socket;
