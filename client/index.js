import React from 'react';
import { render } from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';
import Socket from './Socket';
import WelcomePage from './WelcomePage';
import ChatRoomPage from './ChatRoomPage';

render((
  <Socket server="/">
    <Router history={hashHistory}>
      <Route path="/" component={WelcomePage} />
      <Route path="/room/:name" component={ChatRoomPage} />
    </Router>
  </Socket>
), document.getElementById('chat-app'));
